import React from 'react'
import './Search.css'
//fake the response data stored in database
const demoItems = [
  'Apple',
  'mango',
  'banana',
  'organge',
  'appifizz',
  'mongoose',
]

export function Search() {
  const [key, setKey] = React.useState('')
  const [results, setResult] = React.useState([])
  const handleChange = (event) => {
    setKey(event.target.value)
  }
  React.useEffect(() => {
    const results = demoItems.filter((person) =>
      person.toLowerCase().includes(key)
    )
    setResult(results)
  }, [key])

  return (
    <div className='search'>
      <input type='text' value={key} onChange={handleChange} />
      <ul>
        {results.map((item) => (
          <li>{item}</li>
        ))}
      </ul>
    </div>
  )
}
